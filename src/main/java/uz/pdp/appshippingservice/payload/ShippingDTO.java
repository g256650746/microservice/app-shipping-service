package uz.pdp.appshippingservice.payload;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class ShippingDTO {

    private UUID id;

    private String address;

    private UUID orderId;

    private LocalDateTime createdAt = LocalDateTime.now();
}
