package uz.pdp.appshippingservice.aop;

import lombok.Data;

import java.util.Set;
import java.util.UUID;

@Data
public class UserDTO {

    private UUID id;

    private String name;

    private Set<String> permissions;
}
