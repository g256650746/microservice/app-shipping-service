package uz.pdp.appshippingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppShippingServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppShippingServiceApplication.class, args);
    }

}
