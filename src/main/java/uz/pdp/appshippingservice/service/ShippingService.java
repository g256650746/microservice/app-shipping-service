package uz.pdp.appshippingservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import uz.pdp.appshippingservice.entity.Shipping;
import uz.pdp.appshippingservice.payload.ShippingDTO;
import uz.pdp.appshippingservice.repository.ShippingRepository;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class ShippingService {

    private final ShippingRepository shippingRepository;


    public List<Shipping> all() {
        return shippingRepository.findAll();
    }


    public Shipping add(ShippingDTO shippingDTO) {
        try {
            Thread.sleep(10_000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        Shipping shipping = new Shipping();
        BeanUtils.copyProperties(shippingDTO, shipping);
        shippingRepository.save(shipping);
        return shipping;
    }

    @RabbitListener(queues = {"for-shipping"})
    public void readFromMQ(Message message) {
        System.out.println("LISTEN QILDIK: ");
        System.out.println(message);
    }
}
