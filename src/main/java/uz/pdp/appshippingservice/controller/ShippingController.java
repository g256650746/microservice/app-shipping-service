package uz.pdp.appshippingservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appshippingservice.aop.CheckUser;
import uz.pdp.appshippingservice.entity.Shipping;
import uz.pdp.appshippingservice.payload.ShippingDTO;
import uz.pdp.appshippingservice.service.ShippingService;

import java.util.List;

@RestController
@RequestMapping("/api/shipping/shipping")
@RequiredArgsConstructor
public class ShippingController {

    private final ShippingService shippingService;
    private final Environment environment;

    @CheckUser
    @GetMapping
    public List<Shipping> list() {
        return shippingService.all();
    }

    @CheckUser(permissions = "CREATE_SHIPPING")
    @PostMapping
    public Shipping create(@RequestBody ShippingDTO shippingDTO) {
        return shippingService.add(shippingDTO);
    }

    @GetMapping("/battar")
    public String getBla() {
        System.out.println(environment.getProperty("app.default.page.size"));
        return environment.getProperty("app.default.page.size");
    }
}
